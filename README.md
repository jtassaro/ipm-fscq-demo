# Overview

This is a demo showing how the Iris Generalized Proof Mode (IPM) can be used
with "Crash Hoare Logic", a separation logic developed for FSCQ, a formally
verified filesystem from the MIT PDOS group.  In particular, we re-do the proofs
related to the "buffer cache" from FSCQ using the proof mode.

## Building from source

This project is set up to use opam for managing dependencies.  First add the
required repositories by running:

    opam repo add coq-released https://coq.inria.fr/opam/released
    opam repo add iris-dev https://gitlab.mpi-sws.org/FP/opam-dev.git

Then run `make build-dep` to install the right versions of the dependencies.

Run `make -jN` to build the full development, where `N` is the number of your
CPU cores.

## Outline of contents

Unfortunately, because of inconsistencies in Coq notations between FSCQ and the
stdpp library used by Iris, as well as differing Coq version dependencies, we
cannot just import the FSCQ development for this demo. Instead, the `theories/`
directory contains copies of the files from FSCQ needed for the buffer cache
example, as well as the files needed to instantiate the IPM framework. The key
files are:

* `Pred.v` contains the definition of the separation logic connectives.

* `PredBi.v` has a proof that the connectives form a model of BI, as
  axiomatized in IPM. It also develops a number of tactics for automating
  proofs.

* `Cache.v` this contains the original proofs related to the buffer cache in
  FSCQ.

* `CacheIPM.v` is the same proofs re-done using the IPM.

## Credit

FSCQ was developed by Tej Chajed, Atalay İleri, Alex Konradi, Daniel Ziegler,
Haogang Chen, Prof. M. Frans Kaashoek, Prof. Nickolai Zeldovich, and Prof. Adam
Chlipala and is described in the following publications:

* Verifying a high-performance crash-safe file system using a tree specification. 
Haogang Chen, Tej Chajed, Alex Konradi, Stephanie Wang, Atalay Ileri, Adam Chlipala, M. Frans Kaashoek, and Nickolai Zeldovich. SOSP 2017.

* Using Crash Hoare Logic for certifying the FSCQ file system. 
Haogang Chen, Daniel Ziegler, Tej Chajed, Adam Chlipala, M. Frans Kaashoek, and Nickolai Zeldovich. SOSP 2015

FSCQ Project Page: https://pdos.csail.mit.edu/projects/fscq.html

FSCQ Source: https://github.com/mit-pdos/fscq

