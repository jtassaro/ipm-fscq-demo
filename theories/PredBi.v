Require Import Arith.
Require Import Omega.
Require Import FunctionalExtensionality.
Require Import Mem.
Require Import RelationClasses.
Require Import Morphisms.
Require Import List.
Require Import Pred PredCrash.
From iris Require bi.interface.
From iris Require algebra.ofe.
From Coq.ssr Require Import ssreflect.

Lemma and_sep_lift_empty A1 A2 A3 (P Q: @pred A1 A2 A3) R :
  P /\ (sep_star Q  (lift_empty R)) =p=> sep_star (P /\ Q) (lift_empty R).
Proof.
  unfold_sep_star. intros m (HP&(m1&m2&Hunion&?&HQ&Hemp)).
  exists m, (fun a => None); repeat split; auto.
  * rewrite /mem_union. apply functional_extensionality => a.
    destruct (m a) => //=.
  * rewrite /mem_disjoint. intros (a&v1&v2&?&?). congruence.
  * assert (m = m1) as ->.
    { 
      apply functional_extensionality => a. rewrite Hunion.
      rewrite /mem_union. destruct (m1 _); auto. eapply Hemp.
    }
    done.
  * by destruct Hemp.
Qed.

Global Instance pred_equiv AT AEQ V : base.Equiv (@pred AT AEQ V) := piff.

Global Transparent pred.
Canonical Structure predC AT AEQ V := ofe.discreteC (@pred AT AEQ V).
Program Canonical Structure predI `{AEQ: EqDec AT} V : interface.bi :=
  interface.Bi (@pred AT AEQ V) _ _ pimpl emp (@lift _ _ _) (@and _ _ _)
               (@or _ _ _) (@impl _ _ _) (@foral_ AT AEQ V) (@exis AT AEQ V)
               sep_star (@sepwand _ _ _) (fun P => fun m => P (fun a => None)) _ _.
Import bi.interface.
Next Obligation. intros. apply ofe.discrete_ofe_mixin, _. Qed.
Next Obligation.
  split.
  - split; auto with *.
  - intros; split; auto with *.
  - intros n ?? [? ?]; split; auto.
  - solve_proper. 
  - solve_proper.
  - intros ? P P' [HPP' HP'P] Q Q' [HQQ' HQ'Q]. split.
    * intros m => //=. pred_unfold. intros HPQ HP.
        by apply HQQ', HPQ, HP'P.
    * intros m => //=. pred_unfold. intros HPQ HP.
        by apply HQ'Q, HPQ, HPP'.
  - intros ?? φ φ' Hphi; split; intros m => //=; pred_unfold; intros; by apply Hphi.
  - intros ?? φ φ' Hphi; split; intros m => //=; pred_unfold; intros (?&?); eexists; by eapply Hphi.
  - solve_proper.
  - intros ? P P' [HPP' HP'P] Q Q' [HQQ' HQ'Q]. split.
    * rewrite /sepwand. intros m Hwand m1 [? ?]. apply HQQ'. apply Hwand; split; auto using HPP'.
    * rewrite /sepwand. intros m Hwand m1 [? ?]. apply HQ'Q. apply Hwand; split; auto using HPP'.
  - intros n P Q [HP HQ]; split => m //=; auto using HP, HQ.
  - intros ??? m => //=.
  - intros ?? H m Hphi. apply H; rewrite //=.
  - intros ?? m Hphi. apply Hphi.
  - intros ??? H. apply H. 
  - intros ??? H. apply H. 
  - intros ??? HPQ HPR. by apply pimpl_and_split.
  - intros ??. apply pimpl_or_r; by left.
  - intros ??. apply pimpl_or_r; by right.
  - intros ??. apply pimpl_or_l.
  - intros P Q R Hand m HP HQ. by apply Hand.
  - intros P Q R HPQR m [? ?]; by apply HPQR.
  - intros A P Ψ Hall m HP a. by apply Hall.
  - intros A Ψ a m Hall. apply Hall.
  - intros A Ψ a m Ha. by exists a.
  - intros A Ψ ? H m (a&?). by (apply (H a)).
  - intros ?? ?? HPQ HPQ' m. unfold_sep_star. intros (m1&m2&Hsplit&Hdisj&HP&HP').
    exists m1, m2; split_and!; auto.
  - apply pimpl_star_emp.
  - apply star_emp_pimpl.
  - apply sep_star_comm.
  - apply sep_star_assoc_1.
  - intros P Q R. unfold_sep_star. intros Hsep m HP m' (Hdisj&HQ).
    apply Hsep. exists m, m'; split; auto.
  - intros P Q R. unfold_sep_star. intros Himp m (m1&m2&Hsplit&Hdisj&HP&HP').
    specialize (Himp m1 HP m2). subst. apply Himp; split; auto.
  - intros P Q HPQ m. apply HPQ.
  - auto.
  - intros P m HP => //=.
  - auto. 
  - auto. 
  - unfold_sep_star. intros P Q m (?&?&?&?&?&?). auto. 
  - unfold_sep_star. intros P Q m (HP&HQ).
    exists (fun _ => None), m; split_and!; auto.
    rewrite /mem_disjoint. intros (a&?&?&?&?). congruence.
Qed.
Global Opaque pred.

From iris.proofmode Require Import coq_tactics intro_patterns.
From iris.proofmode Require Import tactics. 
From stdpp Require Import hlist pretty.

Global Instance lift_empty_IntoPure AT AEQ V (P: Prop): IntoPure (@lift_empty AT AEQ V P) P.
Proof.
  rewrite /IntoPure//=. intros m. rewrite /lift_empty. intros (H&?). apply H.
Qed.

Global Instance lift_empty_FromPure AT AEQ V (P: Prop): FromPure true (@lift_empty AT AEQ V P) P.
Proof.
  rewrite /FromPure//=. intros m. rewrite /lift_empty. intros (H&?). split; auto.
Qed.

Global Instance lift_empty_Afine AT AEQ V P: Affine (@lift_empty AT AEQ V P).
Proof.
  rewrite /Affine//=. intros m. rewrite /lift_empty. intros (H&?). rewrite //=.
Qed.

Global Instance lift_empty_into_sep A1 A2 A3 (φ ψ : Prop) :
  IntoSep (@lift_empty A1 A2 A3 (φ ∧ ψ)) (lift_empty φ) (lift_empty ψ).
Proof.
  rewrite /IntoSep.  iStartProof. iIntros "H".  iDestruct "H" as %[? ?]; auto.
Qed.

Global Instance lift_empty_Persistent AT AEQ V P: Persistent (@lift_empty AT AEQ V P).
Proof.
  rewrite /Persistent//=. intros m. rewrite /lift_empty. intros (H&?). rewrite //=.
Qed.

(*  More aggresive sep/exist classes let us easily handle the deeply nested goals
    arising is FSCQ examples. *)
Global Instance into_exist_sep_l {A} A1 A2 A3 (P: @pred A1 A2 A3) Q Φ :
  IntoExist Q Φ → IntoExist (P ∗ Q) (λ a : A, P ∗ Φ a)%I.
Proof.
  rewrite /IntoExist. iIntros (Helim) "(HP&HQ)". iEval (rewrite Helim) in "HQ".
  iDestruct "HQ" as (x) "Hϕ". iExists x. iFrame.
Qed.

Global Instance into_exist_sep_r {A} A1 A2 A3 (P: @pred A1 A2 A3) Q Φ :
  IntoExist Q Φ → IntoExist (Q ∗ P) (λ a : A, Φ a ∗ P)%I.
Proof.
  rewrite /IntoExist. iIntros (Helim) "(HQ&HP)". iEval (rewrite Helim) in "HQ".
  iDestruct "HQ" as (x) "Hϕ". iExists x. iFrame.
Qed.

Global Instance into_exist_and_l {A} A1 A2 A3 (P: @pred A1 A2 A3) Q Φ :
  IntoExist Q Φ → IntoExist (P ∧ Q) (λ a : A, P ∧ Φ a)%I.
Proof.
  rewrite /IntoExist => ->. by rewrite bi.and_exist_l.
Qed.

Global Instance into_exist_and_r {A} A1 A2 A3 (P: @pred A1 A2 A3) Q Φ :
  IntoExist Q Φ → IntoExist (Q ∧ P) (λ a : A, Φ a ∧ P)%I.
Proof.
  rewrite /IntoExist => ->. by rewrite bi.and_exist_r.
Qed.

Global Instance into_exist_sep_pure_r (PROP : bi) (P Q : PROP) (φ : Type):
  IntoPureT P φ → TCOr (Affine P) (Absorbing Q) → IntoExist (Q ∗ P) (λ _ : φ, Q).
Proof.
  intros. rewrite /IntoExist. rewrite comm. iIntros "H". iDestruct "H" as (?) "H". iExists H1. auto.
Qed.

Global Instance from_exist_sep_l {A} A1 A2 A3 (P: @pred A1 A2 A3) Q Φ :
  FromExist Q Φ → FromExist (P ∗ Q) (λ a : A, P ∗ Φ a)%I.
Proof.
  rewrite /FromExist. iIntros (Helim) "H". iDestruct "H" as (a) "(HP&HQ)".
  iFrame. iApply Helim. iExists a. done.
Qed.

Global Instance from_exist_sep_r {A} A1 A2 A3 (P: @pred A1 A2 A3) Q Φ :
  FromExist Q Φ → FromExist (Q ∗ P) (λ a : A, Φ a ∗ P)%I.
Proof.
  rewrite /FromExist. iIntros (Helim) "H". iDestruct "H" as (a) "(HQ&HP)".
  iFrame. iApply Helim. iExists a. done.
Qed.

Ltac ExIntro :=
match goal with
| [ |- ?F ⇨⇨ (exists _ : pred, (_ ✶ _)) ] =>
  iIntros; iExists F; iFrame; iPureIntro; split
end.

Lemma lift_empty_True A1 A2 A3: @lift_empty A1 A2 A3 True ≡ emp%I.
Proof.
  split.
  - intros m; rewrite /lift_empty.  intros (?&?); auto.
  - intros m; rewrite /lift_empty.  intros; split; auto.
Qed.

Ltac iCancelPureRec P :=
  match P with
  | lift_empty ?P' =>
    let H := iFresh in
    iAssert (@lift_empty _ _ _ P') as H; first done; iFrame H
  | (?P1 ✶ ?P2)%pred => try (iCancelPureRec P1); try (iCancelPureRec P2)
  | (?P1 ∗ ?P2)%I => try (iCancelPureRec P1); try (iCancelPureRec P2)
  | (crash_xform ?P)%I => try (iCancelPureRec P)
  end.

Ltac iCancelAndPureRec P :=
  match P with
  | lift_empty ?P' =>
    let H := iFresh in
    iAssert (@lift_empty _ _ _ P') as H; first done; iFrame H
  | (?P1 ✶ ?P2)%pred => try (iCancelAndPureRec P1); try (iCancelAndPureRec P2)
  | (?P1 ∗ ?P2)%I => try (iCancelAndPureRec P1); try (iCancelAndPureRec P2)
  | (?P1 ⋀ ?P2)%pred => try (iCancelAndPureRec P1); try (iCancelAndPureRec P2)
  | (crash_xform ?P)%I => try (iCancelPureRec P)
  end.

Ltac iCancelPure :=
  match goal with
  | [ |- environments.envs_entails _ ?P] => iCancelPureRec P
  end.

Ltac iCancelAndPure :=
  match goal with
  | [ |- environments.envs_entails _ ?P] => iCancelAndPureRec P
  end.


Ltac iDestructRep H :=
  let H1 := iFresh in
  let H2 := iFresh in
  let pat :=constr:(IList [cons (IIdent H1) (cons (IIdent H2) nil)]) in 
  iDestruct H as pat; try (iDestructRep H1); try (iDestructRep H2);
  try (iDestruct H1 as "%"); try (iDestruct H2 as "%").

Ltac iCancelIntro :=
  let H1 := iFresh in
  iIntros H1; iDestructRep H1; iFrame; try iCancelPure; iRevert "∗".

Ltac iDestructExRep H :=
  try (let H1 := iFresh in
       let H2 := iFresh in
       let pat := constr:(IList [cons (IIdent H1) (cons (IIdent H2) nil)]) in 
       first [ (iDestruct H as (?) H; iDestructExRep H)
               || iDestruct H as pat; iDestructExRep H1; iDestructExRep H2;
               try (iDestruct H1 as "%"); try (iDestruct H2 as "%") ]).

Ltac iCancelExIntro :=
  let H1 := iFresh in
  iIntros H1; iDestructExRep H1; repeat iExists _; iFrame; try iCancelPure; iRevert "∗".

Ltac ixCrashRewrite :=
match goal with
| [ H : forall rc hm, (crash_xform rc =p=> crash_xform ?x) -> _ =p=>
    ?c hm |- envs_entails _ (?c ?hm)] => iApply H
| [ H : crash_xform ?rc =p=> _ |- envs_entails _ (crash_xform ?rc =p=> _) ] => rewrite H
| [ H : crash_xform ?rc =p=> _ |- envs_entails _ (crash_xform ?rc -∗ _) ] => rewrite H
| [ H : crash_xform ?rc =p=> _ |- (crash_xform ?rc =p=> _) ] => rewrite H
end.

Global Instance crash_xform_IntoPure (P: Prop): IntoPure (crash_xform (lift_empty P)) P.
Proof.
  rewrite /IntoPure//=. rewrite crash_xform_lift_empty. iIntros "H". by iDestruct "H" as %H.
Qed.

Global Instance crash_xform_FromPure (P: Prop): FromPure true (crash_xform (lift_empty P)) P.
Proof.
  rewrite /FromPure//=. rewrite crash_xform_lift_empty. iIntros "#%". done.
Qed.

Global Instance crash_xform_IntoSep P Q R:
  IntoSep P Q R →
  IntoSep (crash_xform P) (crash_xform Q) (crash_xform R). 
Proof.
  rewrite /IntoSep//= => ->. rewrite crash_xform_sep_star_dist. done.
Qed.

Global Instance crash_xform_FromSep P Q R:
  FromSep P Q R →
  FromSep (crash_xform P) (crash_xform Q) (crash_xform R). 
Proof.
  rewrite /FromSep. iIntros (H).
  rewrite /interface.bi_sep//=  -crash_xform_sep_star_dist H //=.
Qed.

Global Instance crash_xform_Affine P :
  Affine P →
  Affine (crash_xform P).
Proof.
  rewrite /Affine => ->. by rewrite crash_invariant_emp.
Qed.

Lemma modality_crash_mixin:
  modality_mixin (crash_xform) MIEnvId MIEnvIsEmpty.
Proof.
  split.
  - intros P. 
    rewrite /bi_affinely/bi_persistently//= => m HP.
    rewrite /crash_xform. exists m. split; auto.
    apply possible_crash_refl.
    { intros a. left. destruct HP; eauto. } 
  - rewrite //=. 
  - apply crash_invariant_emp_r. 
  - intros. by apply crash_xform_pimpl.
  - intros. iIntros "(HP&?)". iSplitL "HP"; iAssumption.
Qed.
      
Definition modality_crash :=
  Modality _ (modality_crash_mixin). 

Global Instance crash_xform_FromModal P:
  FromModal (modality_crash) True%pred (crash_xform P) P.
Proof.
  rewrite /FromModal => //=.
Qed.

Global Instance crash_Frame_int R P Q :
  Frame true R P Q → Frame true R (crash_xform P) (crash_xform Q).
Proof.
  rewrite /Frame=><- //= m => //=.
  rewrite /bi_persistently/bi_affinely/bi_sep/bi_and//=.
  unfold_sep_star.
  intros (m1&m2&Heq&Hdisj&((Hemp&HR)&HQ)).
  rewrite /crash_xform in HQ *.
  destruct HQ as (m'&(HQ&Hcrash)). exists m'.
  assert (m2 = m) as <-.
  {
      apply functional_extensionality => a. rewrite Heq.
      rewrite /mem_union. specialize (Hemp a). destruct (m1 _); auto. congruence.
  }
  split; auto. exists (empty_mem), m'; split_and!; auto.
  * apply mem_disjoint_empty_mem.
  * split; auto. intros a => //=.
Qed.

Global Instance crash_Frame R P Q :
  Frame false R P Q → Frame false (crash_xform R) (crash_xform P) (crash_xform Q).
Proof.
  rewrite /Frame=><- //= m => //=.
  rewrite /bi_persistently/bi_affinely/bi_sep/bi_and//=.
  apply crash_xform_sep_star_dist.
Qed.

Hint Extern 0 (envs_entails _ (crash_xform (bi_emp))) => iApply crash_invariant_emp_r; iEmpIntro.

(*
Lemma test P Q:
  crash_xform P ∗ crash_xform Q ⊢ crash_xform (P ∗ Q).
Proof.
  iIntros "(HP&HQ)". iFrame. done.
Qed.
*)

Global Instance into_exist_crash_xform {A} Q Φ :
  IntoExist Q Φ → IntoExist (crash_xform Q) (λ a : A, crash_xform (Φ a))%I.
Proof.
  rewrite /IntoExist => ->. rewrite crash_xform_exists_comm. done.
Qed.

Global Instance from_exist_crash_xform {A} Q Φ :
  FromExist Q Φ → FromExist (crash_xform Q) (λ a : A, crash_xform (Φ a))%I.
Proof.
  rewrite /FromExist => Hfrom. rewrite /bi_exist//=. rewrite -crash_xform_exists_comm.
  rewrite Hfrom. done.
Qed.

Tactic Notation "iApply" open_constr(lem) "in" open_constr(H) :=
  let Hfresh := iFresh in
  iRename H into Hfresh; iPoseProof lem as H; iSpecialize (H with Hfresh).
